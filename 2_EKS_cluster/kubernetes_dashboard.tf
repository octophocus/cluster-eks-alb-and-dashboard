module "kubernetes_dashboard" {
  source  = "cookielab/dashboard/kubernetes"
  version = "0.11.0"

  kubernetes_namespace_create     = false
  kubernetes_namespace            = "kube-system"
  kubernetes_service_account_name = "kubernetes-dashboard-user"
  kubernetes_dashboard_csrf       = module.eks.cluster_certificate_authority_data

}
