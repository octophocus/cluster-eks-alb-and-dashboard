module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_version                      = "1.20"
  cluster_name                         = var.clustername
  cluster_endpoint_private_access      = true
  cluster_endpoint_public_access       = true
  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"] #You should restrict access to external IPs that need to access the cluster
  vpc_id                               = data.terraform_remote_state.stage1.outputs.vpc_id
  subnets                              = data.terraform_remote_state.stage1.outputs.all_subnets

  map_users = [
    {
      userarn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.user_to_map}",
      username = var.user_to_map,
      groups   = ["system:masters"]
    }
  ]

  write_kubeconfig = true
  manage_aws_auth  = true

  tags = {
    "kubernetes.io/cluster/${var.clustername}" = "owned"
  }

  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  node_groups = {
    ng-01 = {
      name                   = "Public"
      create_launch_template = true
      public_ip              = true
      instance_types         = ["t2.small", "t2.medium", "t2.large"]
      capacity_type          = "SPOT"
      desired_capacity       = 2
      max_capacity           = 5
      min_capacity           = 1
      root_volume_size       = 100
      root_volume_type       = "gp2"
      ebs_optimized          = false
      enable_monitoring      = true
      subnets                = data.terraform_remote_state.stage1.outputs.pub_subnets
    }
    #ng-02 = {
    #  name                   = "Private"
    #  create_launch_template = true
    #  instance_types         = ["t2.large"]
    #  capacity_type          = ON_DEMAND
    #  desired_capacity       = 1
    #  max_capacity           = 2
    #  min_capacity           = 1
    #  root_volume_size       = 100
    #  root_volume_type       = "gp2"
    #  ebs_optimized          = false
    #  enable_monitoring      = true
    #  subnets = data.terraform_remote_state.stage1.outputs.priv_subnets
    # }
  }
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host  = module.eks.cluster_endpoint
  token = data.aws_eks_cluster_auth.eks.token
  cluster_ca_certificate = base64decode(
    module.eks.cluster_certificate_authority_data
  )
}