# aws_vpc_endpoint.vpce-autoscaling:
resource "aws_vpc_endpoint" "vpce-autoscaling" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.autoscaling", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-ec2:
resource "aws_vpc_endpoint" "vpce-ec2" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.ec2", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-vpce-ec2messages:
resource "aws_vpc_endpoint" "vpce-vpce-ec2messages" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.ec2messages", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-ecrapi:
resource "aws_vpc_endpoint" "vpce-ecrapi" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.ecr.api", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-ecrdkr:
resource "aws_vpc_endpoint" "vpce-ecrdkr" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.ecr.dkr", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-ec2:
resource "aws_vpc_endpoint" "vpce-elb" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.elasticloadbalancing", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-logs:
resource "aws_vpc_endpoint" "vpce-logs" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.logs", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
# aws_vpc_endpoint.vpce-s3:
resource "aws_vpc_endpoint" "vpce-s3" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
      Version = "2008-10-17"
    }
  )
  private_dns_enabled = false
  route_table_ids     = data.terraform_remote_state.stage1.outputs.priv-rtb
  security_group_ids  = []
  service_name        = format("com.amazonaws.%s.s3", data.aws_region.current.name)
  subnet_ids          = []
  tags                = {}
  vpc_endpoint_type   = "Gateway"
  vpc_id              = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}

# aws_vpc_endpoint.vpce-sts:
resource "aws_vpc_endpoint" "vpce-sts" {
  policy = jsonencode(
    {
      Statement = [
        {
          Action    = "*"
          Effect    = "Allow"
          Principal = "*"
          Resource  = "*"
        },
      ]
    }
  )
  private_dns_enabled = true
  route_table_ids     = []
  security_group_ids = [
    module.eks.worker_security_group_id,
    module.eks.cluster_security_group_id
  ]
  service_name = format("com.amazonaws.%s.sts", data.aws_region.current.name)
  subnet_ids   = data.terraform_remote_state.stage1.outputs.priv_subnets

  tags              = {}
  vpc_endpoint_type = "Interface"
  vpc_id            = data.terraform_remote_state.stage1.outputs.vpc_id

  timeouts {}
}
