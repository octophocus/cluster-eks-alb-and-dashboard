provider "helm" {
  kubernetes {
    host  = module.eks.cluster_endpoint
    token = data.aws_eks_cluster_auth.eks.token
    cluster_ca_certificate = base64decode(
      module.eks.cluster_certificate_authority_data
    )
  }
}

# 2 Policies creations from json files
resource "aws_iam_policy" "load_balancer_policy" {
  name        = "AWSLoadBalancerControllerIAMPolicy"
  path        = "/"
  description = "AWS LoadBalancer Controller IAM Policy"

  policy = file("alb_controlller_policy.json")

  depends_on = [module.eks]
}

resource "aws_iam_policy" "load_balancer_policy_additional" {
  name        = "AWSLoadBalancerControllerIAMPolicy_additional"
  path        = "/"
  description = "AWS LoadBalancer Controller IAM Policy with additional permissions"

  policy = file("alb_controller_additional_policy.json")

  depends_on = [module.eks]
}

# 2 Policies attachments to the ALB serviceaccount role
resource "aws_iam_role_policy_attachment" "role_alb" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.load_balancer_policy.arn
}

resource "aws_iam_role_policy_attachment" "role_alb_additional" {
  role       = module.eks.worker_iam_role_name
  policy_arn = aws_iam_policy.load_balancer_policy_additional.arn
}

#Deploy ALB with helm
resource "helm_release" "aws_load_balancer_controller" {
  name       = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  namespace  = "kube-system"

  set {
    name  = "clusterName"
    value = module.eks.cluster_id
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }

  set {
    name  = "image.repository"
    value = format("602401143452.dkr.ecr.%s.amazonaws.com/amazon/aws-load-balancer-controller", var.default_region)
  }

  set {
    name  = "image.tag"
    value = "v2.3.0"
  }

  depends_on = [
    aws_iam_role_policy_attachment.role_alb,
    aws_iam_role_policy_attachment.role_alb_additional
  ]
}

#Update kubeconfig for ou local shell to get it ready!
resource "null_resource" "auth_local_shell" {
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    on_failure = fail
    when       = create
    #interpreter = ["/bin/bash", "-c"]
    command = "rm -f ~/.kube/config && aws eks update-kubeconfig --name ${module.eks.cluster_id} --region ${var.default_region}"
  }
  depends_on = [helm_release.aws_load_balancer_controller]
}
