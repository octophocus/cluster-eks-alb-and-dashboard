####################################### Outputs general #######################################
output "default_region" {
  value = var.default_region
}

output "project_name" {
  value = var.project
}

####################################### Outputs module EKS #######################################

output "cluster_name" {
  value = module.eks.cluster_id
}

output "EKS_endpoint" {
  value = module.eks.cluster_endpoint
}

output "EKS_arn" {
  value = module.eks.cluster_arn
}

output "EKS_ca" {
  value = module.eks.cluster_certificate_authority_data
}

output "EKS_OIDC_arn" {
  value = module.eks.oidc_provider_arn
}

output "EKS_OIDC_url" {
  value = module.eks.cluster_oidc_issuer_url
}

output "worker_sg" {
  value = module.eks.worker_security_group_id
}

output "cluster_sg" {
  value = module.eks.cluster_security_group_id
}

output "worker_role_name" {
  value = module.eks.worker_iam_role_name
}

