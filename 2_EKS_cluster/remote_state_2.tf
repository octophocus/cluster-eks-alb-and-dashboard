data "terraform_remote_state" "stage1" {
  backend = "s3"
  config = {
    bucket = "cluster-eks-alb-<ACCOUNT_ID>-terraform-backend-stage1"
    region = "eu-west-1"
    key    = "cluster-eks-alb-stage1.tfstate"
  }
}