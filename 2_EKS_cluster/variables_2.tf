####################################### Global #######################################
variable "project" {
  default = "cluster-eks-alb"
}

variable "default_region" {
  default = "eu-west-1"
}

variable "az" {
  type    = list(string)
  default = ["eu-west-1a", "eu-west-1b"]
}

####################################### EKS CLuster #######################################
variable "clustername" {
  default = "cluster-eks"
}

variable "user_to_map" {
  type        = string
  description = "Login name of the user to map to our cluster EKS"
}