########################## MAIN for Stack 1 ##########################
terraform {
  required_version = ">= 0.15.0"

  required_providers {
    aws = ">= 3.63.0"
  }

  backend "s3" {
    bucket         = "cluster-eks-alb-<ACCOUNT_ID>-terraform-backend-stage2"
    key            = "cluster-eks-alb-stage2.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "cluster-eks-alb-<ACCOUNT_ID>-terraform-locktable-stage2"
  }
}

provider "aws" {
  region = var.default_region
  default_tags {
    tags = {
      Environment = "Test"
      Terraform   = "true"
      project     = var.project
    }
  }
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_availability_zones" "az" {
  state = "available"
}


