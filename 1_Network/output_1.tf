####################################### Outputs general #######################################
output "default_region" {
  value = var.default_region
}

output "project_name" {
  value = var.project
}

####################################### Outputs network #######################################
output "vpc_id" {
  value = module.vpc_eks.vpc_id
}

output "all_subnets" {
  value = data.aws_subnet_ids.all.ids
}

output "priv_subnets" {
  value = module.vpc_eks.private_subnets
}

output "priv_subnets_cidrs" {
  value = module.vpc_eks.private_subnets_cidr_blocks
}

output "pub_subnets" {
  value = module.vpc_eks.public_subnets
}

output "priv_rtb" {
  value = module.vpc_eks.private_route_table_ids
}

output "pub_rtb" {
  value = module.vpc_eks.public_route_table_ids
}

output "IGW" {
  value = module.vpc_eks.igw_id
}

output "priv-rtb" {
  value = module.vpc_eks.private_route_table_ids
}