module "vpc_eks" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.0"

  name = "vpc_EKS_cluster"
  cidr = "10.0.0.0/16"

  azs             = var.azs
  private_subnets = var.priv_subnets
  public_subnets  = var.pub_subnets

  enable_nat_gateway   = false #private network use VPC Endpoint instead
  enable_dns_hostnames = true
  enable_dns_support   = true

  private_subnet_tags = {
    Name                              = "Private-subnets"
    "kubernetes.io/role/internal-elb" = 1
  }

  public_subnet_tags = {
    Name                     = "Public-subnets"
    "kubernetes.io/role/elb" = 1
  }

  igw_tags = {
    Name = "Internet_GTW"
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = module.vpc_eks.vpc_id

  depends_on = [module.vpc_eks]
}