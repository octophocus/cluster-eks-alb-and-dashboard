####################################### Global #######################################
variable "project" {
  default = "cluster-eks-alb"
}

variable "default_region" {
  default = "eu-west-1"
}

variable "azs" {
  type    = list(string)
  default = ["eu-west-1a", "eu-west-1b"]
}
####################################### VPC #######################################
variable "priv_subnets" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "List of private subnets"
}

variable "pub_subnets" {
  type        = list(string)
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
  description = "List of public subnets"
}