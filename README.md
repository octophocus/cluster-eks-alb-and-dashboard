#Cluster EKS with kubernetes dashboard and ALB controller

![This is an image](https://gitlab.com/octophocus/cluster-eks-alb-and-dashboard/-/raw/main/Final_architecture.jpg)

Summary:
- Prerequisites
- Initialize Terraform artefacts state
- Deployment
- Connect to the Kubeernetes-dashboard
- Test your ALB with the Game2048 deployment
_______________________________________________________________________________________________________________________________________________
**Prerequisites**

| Name | Version | Link |
| ------ | ------ | ------ |
| Terraform | 0.15.0 | https://www.terraform.io/downloads.html |
| awscli | 2.2.34 | https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html |
| Kubectl | 1.21 | https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/ |
_______________________________________________________________________________________________________________________________________________

**Initialize Terraform artefacts state**

```shell
cd 0_TF_init/
./TF_init.sh
```
This step creates a backend for each stages of our deployment. We can pass values or variables from stage 1 to stage 2 via terraform remote state outputs (see in the remote_state_2.tf).
_______________________________________________________________________________________________________________________________________________

**Deployment**

After backend initialization we can deploy our environement.

STAGE1
```shell
cd 1_Network/
terraform init
terraform fmt
terraform validate
terraform apply
```
Terraform will do the provisionning network stack specified. For this step I used a VPC module.
| module | version | Link |
| ------ | ------ | ------ |
| vpc | 3.11.0 |  | https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest?tab=inputs |

STAGE2
```shell
cd 2_EKS_cluster/
terraform init
terraform fmt
terraform validate
terraform apply
```
Terraform will continues with all the EKS/ALB/dashboard ressources via modules and helm.
| module | version | Link |
| ------ | ------ | ------ |
| eks | 17.24.0 | https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest?tab=inputs |
| kubernetes_dashboard | 0.11.0 | https://registry.terraform.io/modules/cookielab/dashboard/kubernetes/latest?tab=inputs |

The provider helm for terraform perform the ALB deployment. 
This link (https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html) provide the official AWS documentation to deploy the ALB without Terraform simplicity. It helps to understand in depth the helm install and specifications.
_______________________________________________________________________________________________________________________________________________

**Connect to the Kubeernetes-dashboard**

This section it's an extract from the official AWS documentation: https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html

1) Retrieve your token for the user service account with this command: (we deploy the module dashboard with the user 'kubernetes-dashboard-user', see the file kubernetes_dashboard.tf)
```shell
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep kubernetes-dashboard-user | awk '{print $1}')
```

2) Run on your local shell 
```shell
kubectl proxy
```
To access the dashboard endpoint, open this link on your local browser: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

3) choose token, paste the authentication_token and then 'sign in'.
_______________________________________________________________________________________________________________________________________________

**Test your ALB with the Game2048 deployment**

To test our ALB controller we can do a kubernetes deployment of game usually used in many articles and workshop.
```shell
 kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.3.0/docs/examples/2048/2048_full.yaml
 kubectl get ingress/ingress-2048 -n game-2048

 #to delete the deployment
 kubectl delete -f https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.3.0/docs/examples/2048/2048_full.yaml
```
This last command should return the address of the application endpoint.

In case of malfunction try this to show ALB logs:
```shell
kubectl logs -n kube-system   deployment.apps/aws-load-balancer-controller
```
_______________________________________________________________________________________________________________________________________________
Others usefull links:
https://tf-eks-workshop.workshop.aws/000_workshop_introduction.html
https://dev.to/stack-labs/deploying-production-ready-gitlab-on-amazon-eks-with-terraform-3coh
https://jarombek.com/blog/sep-28-2020-eks
https://github.com/awslabs/amazon-eks-ami/blob/master/files/eni-max-pods.txt
https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html
