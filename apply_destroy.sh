#\bin\sh
#Script to help apply or destroy more easily all ressources in dev step
#to apply : ./apply_destroy.sh apply
#to destroy: ./apply_destroy.sh destroy

set -e

echo "select $1"

PATH_PROJECT="$HOME/Git/project/cluster-eks-alb-and-dashboard"
STAGE1="$PATH_PROJECT/1_Network/"
STAGE2="$PATH_PROJECT/2_EKS_clsuter/"

function TF_apply() {
    terraform init
    terraform fmt
    terraform validate
    terraform apply -auto-approve
}

function TF_destroy() {
    terraform init
    terraform fmt
    terraform validate
    terraform destroy -auto-approve
}

case "$1" in 
    apply )
        echo "$(tput setab 3; tput setaf 0) applying stage 1 $(tput sgr0)"
        cd $STAGE1
        TF_apply
        echo "$(tput setab 2; tput setaf 0) applyed stage 1 $(tput sgr0)"

        echo "$(tput setab 3; tput setaf 0) applying stage 2 $(tput sgr0)"
        cd $STAGE2
        TF_apply 
        echo "$(tput setab 2; tput setaf 0) applyed stage 2 $(tput sgr0)"
        ;;

    destroy)
        echo "$(tput setab 3; tput setaf 0) destroying stage 2 $(tput sgr0)"
        cd $STAGE2
        TF_destroy 
        echo "$(tput setab 2; tput setaf 0) destroyed stage 2 $(tput sgr0)"

        echo "$(tput setab 3; tput setaf 0) destroying stage 1 $(tput sgr0)"
        cd $STAGE1
        TF_destroy
        echo "$(tput setab 2; tput setaf 0) destroyed stage 1 $(tput sgr0)"
        ;;
esac

exit


