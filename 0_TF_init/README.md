**Stage 0 - TF init**

**IMPORTANT:** Set your own accountID in the name of backend in: 
- main_1.tf 
- main_2.tf
- remote_state_2.tf 

```shell
  backend "s3" {
    bucket         = "cluster-eks-alb-<ACCOUNT_ID>-terraform-backend-stage1"
    key            = "cluster-eks-alb-stage1.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "cluster-eks-alb-<ACCOUNT_ID>-terraform-locktable-stage1"
  }
  ```

If you modify the name of the Terraform state file and lock table in the TF_init.sh, you have to apply changes in the main and remote_state files too (backend ressource do not accept variable).
