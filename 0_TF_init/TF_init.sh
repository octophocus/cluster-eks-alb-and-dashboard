#\bin\sh
set -e

export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)
export AWS_REGION=eu-west-1
export PROJECT=cluster-eks-alb
export TERRAFORM_BUCKET_NAME=${PROJECT}-${AWS_ACCOUNT_ID}-terraform-backend
export TERRAFORM_DYNAMO_TABLE=${PROJECT}-${AWS_ACCOUNT_ID}-terraform-locktable

#STAGE 1
### S3 ###
# Create bucket
aws s3api create-bucket \
     --bucket $TERRAFORM_BUCKET_NAME-stage1 \
     --region $AWS_REGION \
     --create-bucket-configuration LocationConstraint=$AWS_REGION

# Make it not public     
aws s3api put-public-access-block \
    --bucket $TERRAFORM_BUCKET_NAME-stage1 \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

# Enable versioning
aws s3api put-bucket-versioning \
    --bucket $TERRAFORM_BUCKET_NAME-stage1 \
    --versioning-configuration Status=Enabled

### DynamoDB ###
aws dynamodb create-table \
    --table-name $TERRAFORM_DYNAMO_TABLE-stage1 \
    --attribute-definitions AttributeName=LockID,AttributeType=S \
    --key-schema AttributeName=LockID,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5


#STAGE 2
### S3 ###
# Create bucket
aws s3api create-bucket \
     --bucket $TERRAFORM_BUCKET_NAME-stage2 \
     --region $AWS_REGION \
     --create-bucket-configuration LocationConstraint=$AWS_REGION

# Make it not public     
aws s3api put-public-access-block \
    --bucket $TERRAFORM_BUCKET_NAME-stage2 \
    --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

# Enable versioning
aws s3api put-bucket-versioning \
    --bucket $TERRAFORM_BUCKET_NAME-stage2 \
    --versioning-configuration Status=Enabled

### DynamoDB ###
aws dynamodb create-table \
    --table-name $TERRAFORM_DYNAMO_TABLE-stage2 \
    --attribute-definitions AttributeName=LockID,AttributeType=S \
    --key-schema AttributeName=LockID,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5